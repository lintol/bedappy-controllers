<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\Transformers\ExampleTransformer;
use Illuminate\Database\Eloquent\Collection;

class ExampleController extends Controller
{
    /**
     * Transformer to turn Example into API ready format.
     *
     * @var App\Transformers\ExampleTransformer;
     */
    protected $transformer;

    public function __construct()
    {
        $this->transformer = new ExampleTransformer(false);
        $this->shortTransformer = new ExampleTransformer(true);
    }

    public function index(Request $request)
    {
        $user = Auth::user();

        $examples = Example::accessibleBy($user)->get();

        return fractal($examples, $this->shortTransformer)
            ->respond();
    }

    public function show(Request $request, $id)
    {
        $example = Example::findOrFail($id);

        $this->authorize('view', $example);

        return fractal($example, $this->transformer)
            ->respond();
    }

    public function update(Request $request, $id)
    {
        $example = Example::findOrFail($id);

        $this->authorize('update', $example);

        $request->replace(ExampleTransformer::mapInput($request->all()));

        $request = $this->sanitize($request, Example::filters());

        $this->validate($request, Example::rules());

        $example->fill($request->all());

        $example->save();

        return fractal($example, $this->transformer)
            ->respond();
    }

    public function destroy(Request $request, $id)
    {
        $example = Example::findOrFail($id);

        $this->authorize('delete', $example);

        $example->delete();

        return "Example destroyed";
    }

    public function store(Request $request)
    {
        $this->authorize('create', Example::class);

        $request->replace(ExampleTransformer::mapInput($request->all()));

        $request = $this->sanitize($request, Example::filters());

        $this->validate($request, Example::rules());

        $example = Example::create($request->all());

        return fractal($example, $this->transformer)
            ->respond();
    }
}
