<?php namespace App\Transformers;

use League\Fractal;

class Transformer extends Fractal\TransformerAbstract
{
    /**
     * List of resources to include in the short version by default
     *
     * @var array
     */
    protected $defaultShortIncludes = [
    ];

    /**
     * Build the transformer
     *
     * @param bool $shortVersion Make it concise
     */
    public function __construct($shortVersion = false)
    {
        if ($shortVersion) {
            $this->defaultIncludes = $this->defaultShortIncludes;
        }

        $this->shortVersion = $shortVersion;
    }

    /**
     * Whether or not we should slim down the JSON
     *
     * @var bool
     */
    public $shortVersion = false;

    /**
     * Function to map input to a more useful array
     *
     * @param array
     * @return array
     */
    public static function mapInput(array $data)
    {
        $mapping = static::inputMapping();

        $filteredData = [];
        foreach ($mapping as $field => $key) {
            if (array_key_exists($field, $data)) {
                $entry = $data[$field];
                if (is_callable($key)) {
                    $entries = $key($entry);
                } else {
                    $entries = [$key => $entry];
                }

                if ($entries) {
                    $filteredData = array_merge($filteredData, $entries);
                }
            }
        }

        return $filteredData;
    }

    /**
     * Mapping array for this transformer
     *
     * @return array
     */
    public static function inputMapping()
    {
        return [];
    }
}
