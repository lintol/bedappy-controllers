<?php namespace App\Transformers;

use League\Fractal;
use App\Transformers\Transformer;

class ExampleTransformer extends Transformer
{
    /**
     * List of resources to include by default
     *
     * @var array
     */
    protected $defaultIncludes = [
    ];

    /**
     * List of resources to include by default in the short version
     *
     * @var array
     */
    protected $defaultShortIncludes = [
    ];

    public static function inputMapping()
    {
        return [
            'field_1' => 'fieldInJson',
            'compound_field' => function ($compoundField) {
                return ['compound_field_name' => $compoundField['name']];
            }
        ];
    }

    public function transform(Model $model)
    {
        $data = [
            'field1' => $model->field_1,
        ];

        if (!$this->shortVersion) {
            $data['compoundField'] = [
                'name' => $model->compound_field_name,
            ];
        }
    }
}
